package domain

final case class User(id: Long, name: String)

final case class UserNotFoundException(id: Long) extends Exception
