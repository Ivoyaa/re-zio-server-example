package db

import domain.User
import zio.RIO

object Storage {
  trait Service[R] {
    def get(id: Int): RIO[R, User]
    def create(user: User): RIO[R, User]
    def delete(id: Int): RIO[R, Boolean]
  }