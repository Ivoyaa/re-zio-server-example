package db


import cats.effect.Blocker
import config.Configuration
import domain.{User, UserNotFoundException}
import doobie._
import doobie.implicits.toSqlInterpolator
import doobie.util.transactor.Transactor
import doobie.implicits._
import zio.interop.catz._
import zio.{RIO, Reservation, Task, ZIO, ZLayer}
import config.Configuration.DbConfig
import doobie.h2.H2Transactor
import zio._
import zio.blocking.Blocking

import scala.concurrent.ExecutionContext


private class StorageH2Impl(transactor: Transactor[Task]) extends Storage.Service[Any] {
  import db.StorageH2Impl.SQL

  def get(id: Int): Task[User] = {
    SQL
      .get(id)
      .option
      .transact(transactor)
      .foldM(
        err => Task.fail(err),
        maybeUser => Task.require(UserNotFoundException(id))(Task.succeed(maybeUser))
      )

  }

   def create(user: User): Task[User] = {
    SQL
      .create(user)
      .run
      .transact(transactor)
      .foldM(
        err => Task.fail(err),
        success => Task.succeed(user)
      )
   }

  override def delete(id: Int): Task[Boolean] = {
    SQL
      .delete(id)
      .run
      .transact(transactor)
      .fold(
        err => false,
        success => true
      )
  }

}

object StorageH2Impl {
  object SQL {
    def get(id: Long): doobie.Query0[User] =
      sql"""SELECT * FROM USERS WHERE ID = $id""".query[User]

    def create(user: User): doobie.Update0 =
      sql"""INSERT INTO USERS (id,name) VALUES (${user.id}, ${user.name})""".update

    def delete(id: Long): doobie.Update0 =
      sql"""DELETE FROM USERS WHERE ID = $id""".update
  }

  def makeTransactor(conf: DbConfig,
                     connectEC: ExecutionContext,
                     transactEC: ExecutionContext) = {
    H2Transactor
      .newH2Transactor[Task](
        conf.url,
        conf.user,
        conf.password,
        connectEC,
        Blocker.liftExecutionContext(transactEC)
      )
      .toManagedZIO
  }

  val transactorLive: ZLayer[Blocking with Has[DbConfig], Throwable, Has[H2Transactor[Task]]] =
    ZLayer.fromManaged(for {
      config     <- Configuration.dbConfig.toManaged_
      connectEC  <- ZIO.descriptor.map(_.executor.asEC).toManaged_
      blockingEC <- blocking.blocking { ZIO.descriptor.map(_.executor.asEC) }.toManaged_
      transactor <- makeTransactor(config, connectEC, blockingEC)
    } yield transactor)

}


