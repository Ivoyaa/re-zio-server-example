package config

import pureconfig.{ConfigSource, loadConfigOrThrow}
import zio.{Has, RIO, Task, ULayer, URIO, ZIO, ZLayer}

object Configuration {

  type Configuration = Has[Service]
  case class Config(api: ApiConfig, dbConfig: DbConfig)
  case class ApiConfig(endpoint: String, port: Int)
  case class DbConfig(url: String, user: String, password: String)

  trait Service {
    val load: Task[Config]
  }

  trait Live extends Configuration.Service {
    import pureconfig.generic.auto._

    val load: Task[Config] = Task.effect(ConfigSource.default.loadOrThrow[Config])
  }

  val apiConfig: URIO[Has[ApiConfig], ApiConfig] = ZIO.access(_.get)
  val dbConfig: URIO[Has[DbConfig], DbConfig]    = ZIO.access(_.get)

  val live: ULayer[Configuration] = ZLayer.succeed(new Live{})

  def loadConfig: RIO[Configuration, Config] = RIO.accessM(_.get.load)

}
