name := "re-zio-server-example"

version := "0.1"

scalaVersion := "2.13.6"

val ZIOVersion = "1.0.8"

val Http4sVersion = "0.21.24"

val DoobieVarsion = "0.12.1"

libraryDependencies ++= Seq(
  "dev.zio" %% "zio" % ZIOVersion,
  "dev.zio" %% "zio-interop-cats" % "2.5.1.0",

  "org.http4s" %% "http4s-blaze-server" % Http4sVersion,
  "org.http4s" %% "http4s-circe" % Http4sVersion,
  "org.http4s" %% "http4s-dsl" % Http4sVersion,

  "org.tpolecat" %% "doobie-core" % DoobieVarsion,
  "org.tpolecat" %% "doobie-h2" % DoobieVarsion,

  "com.github.pureconfig" %% "pureconfig" % "0.15.0",
)